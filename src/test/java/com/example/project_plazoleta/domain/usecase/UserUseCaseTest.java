package com.example.project_plazoleta.domain.usecase;

import com.example.project_plazoleta.domain.exception.InvalidUserParameterException;
import com.example.project_plazoleta.domain.model.User;
import com.example.project_plazoleta.domain.spi.UserPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class UserUseCaseTest {

    User user = new User();
    @InjectMocks
    private UserUseCase userUseCaseMock;

    @Mock
    private UserPersistencePort userPersistencePortMock;

    @BeforeEach
    void setup() {

        userUseCaseMock = new UserUseCase(userPersistencePortMock);

        user.setName("Alberto");
        user.setLastName("Carmona");
        user.setDocument("210886677");
        user.setCellphone("+573038966");
        user.setDateBirth(LocalDate.parse("1997-02-02"));
        user.setEmail("testimplement@test.com");
        user.setPassword("Heavy!password123");;
    }

    @Test
    void successfulSavingOwnerFromAdmin() {

        userUseCaseMock.saveOwnerFromAdmin(user);

        Mockito.verify(userPersistencePortMock).saveOwnerFromAdmin(user);
    }

    @Test
    void successfulSavingWorkerFromOwner() {

        userUseCaseMock.saveWorkerFromOwner(user);

        Mockito.verify(userPersistencePortMock).saveWorkerFromOwner(user);
    }

    @Test
    void failSavingUserForWrongEmail_InvalidUserParameterException(){

        user.setEmail("testgmail.com"); // INVALID EMAIL

        assertThrows(InvalidUserParameterException.class, () -> userUseCaseMock.saveOwnerFromAdmin(user));

        Mockito.verify(userPersistencePortMock, Mockito.never()).saveOwnerFromAdmin(any());

    }

    @Test
    void failSavingUserForWrongPhone_InvalidUserParameterException(){

        user.setCellphone("123"); // INVALID PHONE

        assertThrows(InvalidUserParameterException.class, () -> {
            userUseCaseMock.saveOwnerFromAdmin(user);
        });

        Mockito.verify(userPersistencePortMock, Mockito.never()).saveOwnerFromAdmin(any());

    }

    @Test
    void minimumAgeValidation_InvalidUserParameterException(){

        user.setDateBirth(LocalDate.parse("2012-02-02")); // LOW AGE

        assertThrows(InvalidUserParameterException.class, () -> {
            userUseCaseMock.saveOwnerFromAdmin(user);
        });

        Mockito.verify(userPersistencePortMock, Mockito.never()).saveOwnerFromAdmin(any());

    }


    @Test
    void successfulGettingAllUsers(){

        List<User> expectedUsers = new ArrayList<>();

        Mockito.when(userPersistencePortMock.getAllUsers()).thenReturn(expectedUsers);

        List<User> actualUsers = userUseCaseMock.getAllUsers();

        assertEquals(expectedUsers, actualUsers, "Lists are not the same");

    }
}