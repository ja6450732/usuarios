package com.example.project_plazoleta.infrastructure.out.jpa.adapter;

import com.example.project_plazoleta.domain.model.User;
import com.example.project_plazoleta.infrastructure.exception.UserAlreadyExistsException;
import com.example.project_plazoleta.infrastructure.out.jpa.entity.UserEntity;
import com.example.project_plazoleta.infrastructure.out.jpa.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserJpaAdapterTest {

    @InjectMocks
    private UserJpaAdapter userJpaAdapter;

    @Mock
    private UserRepository userRepository;

    @Test
    void failedSavingOwnerFromAdmin_UserAlreadyExistsException() {

        User user = new User();
        user.setEmail("mickhail.kalam@armitage.com");

        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(new UserEntity()));

        assertThrows(UserAlreadyExistsException.class, () -> userJpaAdapter.saveOwnerFromAdmin(user));

    }
}