package com.example.project_plazoleta.application.dto.request;

import lombok.Data;

@Data
public class UserLogInRequestDto {

    private String userName;

    private String password;

}
