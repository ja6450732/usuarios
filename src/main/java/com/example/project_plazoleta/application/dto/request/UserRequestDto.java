package com.example.project_plazoleta.application.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserRequestDto {

    private String name;

    private String lastName;

    private String document;

    private String cellphone;

    private LocalDate dateBirth;

    private String email;

    private String password;

    private Long idRol;


}
