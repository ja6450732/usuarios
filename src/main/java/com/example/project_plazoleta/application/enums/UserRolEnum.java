package com.example.project_plazoleta.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserRolEnum {

    ADMIN(1L),

    OWNER(2L),

    WORKER(3L),

    CLIENT(4L);

    public final Long userRol;

}
