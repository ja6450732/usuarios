package com.example.project_plazoleta.application.mapper;

import com.example.project_plazoleta.application.dto.request.UserRequestDto;
import com.example.project_plazoleta.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface UserRequestMapper {

    @Mapping(source = "idRol", target = "rol.id")
    User toUser(UserRequestDto userRequestDto);
}
