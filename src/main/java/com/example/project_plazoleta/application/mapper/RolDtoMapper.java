package com.example.project_plazoleta.application.mapper;

import com.example.project_plazoleta.application.dto.response.RolDto;
import com.example.project_plazoleta.domain.model.Rol;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface RolDtoMapper {
    RolDto toDto(Rol rol);
    Rol toEntity(RolDto rolDto);
}
