package com.example.project_plazoleta.application.handler.impl;

import com.example.project_plazoleta.application.dto.request.UserRequestDto;
import com.example.project_plazoleta.application.dto.response.UserResponseDto;
import com.example.project_plazoleta.domain.model.User;

import java.util.List;

public interface UserHandlerInterface {

    void saveOwnerFromAdmin(UserRequestDto userRequestDto);

    void saveWorkerFromOwner(UserRequestDto userRequestDto);

    void saveClient(UserRequestDto userRequestDto);

    List<UserResponseDto> getAllUsers();

    UserResponseDto getUserById(Long id);

    UserResponseDto getUserByEmail(String email);

    String validateUser(String email, String password);




}



