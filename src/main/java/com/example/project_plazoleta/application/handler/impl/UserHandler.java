package com.example.project_plazoleta.application.handler.impl;

import com.example.project_plazoleta.application.dto.response.UserResponseDto;
import com.example.project_plazoleta.application.mapper.UserRequestMapper;
import com.example.project_plazoleta.application.mapper.UserResponseMapper;
import com.example.project_plazoleta.infrastructure.configuration.securityconfig.JwtTokenProvider;
import com.example.project_plazoleta.domain.api.UserServicePort;
import com.example.project_plazoleta.domain.exception.InvalidPasswordException;
import com.example.project_plazoleta.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.project_plazoleta.application.dto.request.UserRequestDto;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class UserHandler implements UserHandlerInterface{

    private final UserServicePort userServicePort;

    private final UserResponseMapper userResponseMapper;

    private final UserRequestMapper userRequestMapper;

    private final PasswordEncoder passwordEncoder;

    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public void saveOwnerFromAdmin(UserRequestDto userRequestDto) {
        User user = userRequestMapper.toUser(userRequestDto);
        userServicePort.saveOwnerFromAdmin(user);
    }

    @Override
    public void saveWorkerFromOwner(UserRequestDto userRequestDto) {
        User user = userRequestMapper.toUser(userRequestDto);
        userServicePort.saveWorkerFromOwner(user);
    }

    @Override
    public void saveClient(UserRequestDto userRequestDto) {
        User user = userRequestMapper.toUser(userRequestDto);
        userServicePort.saveClient(user);
    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        return userResponseMapper.toResponseList(userServicePort.getAllUsers());
    }

    @Override
    public UserResponseDto getUserById(Long id) {
        return userResponseMapper.toResponse(userServicePort.getUserById(id));
    }

    @Override
    public UserResponseDto getUserByEmail(String email) {
        return userResponseMapper.toResponse(userServicePort.getUserByEmail(email));
    }

    @Override
    public String validateUser(String email, String password) {

        User user = userServicePort.getUserByEmail(email);

        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new InvalidPasswordException("Invalid password");
        }

        String tokenGenerated = jwtTokenProvider.createToken(email,user.getRol().getId());

        return tokenGenerated;
    }
}
