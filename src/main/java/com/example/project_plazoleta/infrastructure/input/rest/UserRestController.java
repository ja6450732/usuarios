package com.example.project_plazoleta.infrastructure.input.rest;

import com.example.project_plazoleta.application.dto.request.UserLogInRequestDto;
import com.example.project_plazoleta.application.dto.request.UserRequestDto;
import com.example.project_plazoleta.application.dto.response.UserResponseDto;
import com.example.project_plazoleta.application.enums.UserRolEnum;
import com.example.project_plazoleta.application.handler.impl.UserHandler;
import com.example.project_plazoleta.domain.exception.InvalidUserParameterException;
import com.example.project_plazoleta.domain.model.User;
import feign.Response;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserRestController {

    private final UserHandler userHandler;

    private final PasswordEncoder passwordEncoder;

    @PostMapping("/admin/save-owner")
    public ResponseEntity<UserRequestDto> saveOwnerFromAdmin(
            @RequestParam String name,
            @RequestParam String lastName,
            @RequestParam String document,
            @RequestParam String cellphone,
            @RequestParam String dateBirth,
            @RequestParam String email,
            @RequestParam String password
    ) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        UserRequestDto userRequestDto = new UserRequestDto();

        try{
            LocalDate dateBirthParsed = LocalDate.parse(dateBirth, dateTimeFormatter);
            userRequestDto.setDateBirth(dateBirthParsed);

        } catch (DateTimeParseException error) {
            throw new InvalidUserParameterException("Enter a valid dateBirth, the format is YYYY-MM-DD");

        }

        userRequestDto.setName(name);
        userRequestDto.setLastName(lastName);
        userRequestDto.setDocument(document);
        userRequestDto.setCellphone(cellphone);
        userRequestDto.setEmail(email);
        userRequestDto.setPassword(passwordEncoder.encode(password));
        userRequestDto.setIdRol(UserRolEnum.OWNER.getUserRol());

        userHandler.saveOwnerFromAdmin(userRequestDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(userRequestDto);
    }

    @PostMapping("/owner/save-worker")
    public ResponseEntity<UserRequestDto> saveWorkerFromOwner(
            @RequestParam String name,
            @RequestParam String lastName,
            @RequestParam String document,
            @RequestParam String cellphone,
            @RequestParam String dateBirth,
            @RequestParam String email,
            @RequestParam String password
    ) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        UserRequestDto userRequestDto = new UserRequestDto();

        try{
            LocalDate dateBirthParsed = LocalDate.parse(dateBirth, dateTimeFormatter);
            userRequestDto.setDateBirth(dateBirthParsed);

        } catch (DateTimeParseException error) {
            throw new InvalidUserParameterException("Enter a valid dateBirth, the format is YYYY-MM-DD");
        }

        userRequestDto.setName(name);
        userRequestDto.setLastName(lastName);
        userRequestDto.setDocument(document);
        userRequestDto.setCellphone(cellphone);
        userRequestDto.setEmail(email);
        userRequestDto.setPassword(passwordEncoder.encode(password));
        userRequestDto.setIdRol(UserRolEnum.WORKER.getUserRol());

        userHandler.saveWorkerFromOwner(userRequestDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(userRequestDto);
    }

    @PostMapping("/owner/save-worker")
    public ResponseEntity<UserRequestDto> saveClient(
            @RequestParam String name,
            @RequestParam String lastName,
            @RequestParam String document,
            @RequestParam String cellphone,
            @RequestParam String dateBirth,
            @RequestParam String email,
            @RequestParam String password
    ) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        UserRequestDto userRequestDto = new UserRequestDto();

        try{
            LocalDate dateBirthParsed = LocalDate.parse(dateBirth, dateTimeFormatter);
            userRequestDto.setDateBirth(dateBirthParsed);

        } catch (DateTimeParseException error) {
            throw new InvalidUserParameterException("Enter a valid dateBirth, the format is YYYY-MM-DD");
        }

        userRequestDto.setName(name);
        userRequestDto.setLastName(lastName);
        userRequestDto.setDocument(document);
        userRequestDto.setCellphone(cellphone);
        userRequestDto.setEmail(email);
        userRequestDto.setPassword(passwordEncoder.encode(password));
        userRequestDto.setIdRol(UserRolEnum.CLIENT.getUserRol());

        userHandler.saveClient(userRequestDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(userRequestDto);
    }


    @GetMapping("/owner/get-all-users")
    public ResponseEntity<List<UserResponseDto>> getAllUsers() {

        return ResponseEntity.ok(userHandler.getAllUsers());
    }

    @GetMapping("/owner/{idUser}")
    public ResponseEntity<UserResponseDto> getUserById(@PathVariable Long idUser) {
        return ResponseEntity.ok(userHandler.getUserById(idUser));
    }

    @GetMapping("/owner/email={email}")
    public ResponseEntity<UserResponseDto> getUserByEmail(@PathVariable String email) {
        return ResponseEntity.ok(userHandler.getUserByEmail(email));
    }

    @GetMapping("/get-user-token")
    public String validateUser(@RequestParam String email, @RequestParam String password) {

        return userHandler.validateUser(email, password);
    }

}
