package com.example.project_plazoleta.infrastructure.out.jpa.repository;

import com.example.project_plazoleta.infrastructure.out.jpa.entity.RolEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolRepository extends JpaRepository<RolEntity, Long> {
}
