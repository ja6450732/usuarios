package com.example.project_plazoleta.infrastructure.out.jpa.adapter;


import com.example.project_plazoleta.domain.model.Rol;
import com.example.project_plazoleta.domain.spi.RolPersistencePort;
import com.example.project_plazoleta.infrastructure.exception.NoDataFoundException;
import com.example.project_plazoleta.infrastructure.out.jpa.entity.RolEntity;
import com.example.project_plazoleta.infrastructure.out.jpa.mapper.RolEntityMapper;
import com.example.project_plazoleta.infrastructure.out.jpa.repository.RolRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class RolJpaAdapter implements RolPersistencePort {

    private final RolRepository rolRepository;

    private final RolEntityMapper rolEntityMapper;

    @Override
    public List<Rol> getAllRoles() {
        List<RolEntity> rolEntityList = rolRepository.findAll();
        if (rolEntityList.isEmpty()) {
            throw new NoDataFoundException();
        }
        return rolEntityMapper.toRolList(rolEntityList);
    }


}
