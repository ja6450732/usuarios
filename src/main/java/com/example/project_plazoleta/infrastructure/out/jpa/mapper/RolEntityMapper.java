package com.example.project_plazoleta.infrastructure.out.jpa.mapper;

import com.example.project_plazoleta.domain.model.Rol;
import com.example.project_plazoleta.infrastructure.out.jpa.entity.RolEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface RolEntityMapper {

    RolEntity toEntity(Rol rol);

    Rol toRol(RolEntity typeEntity);

    List<Rol> toRolList(List<RolEntity> rolEntityList);


}
