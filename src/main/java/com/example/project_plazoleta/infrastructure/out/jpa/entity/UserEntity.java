package com.example.project_plazoleta.infrastructure.out.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "users")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String lastName;

    private String document;

    private String cellphone;

    private LocalDate dateBirth;

    private String email;

    private String password;

    @ManyToOne
    @JoinColumn(name = "rol")
    private RolEntity rol;

}
