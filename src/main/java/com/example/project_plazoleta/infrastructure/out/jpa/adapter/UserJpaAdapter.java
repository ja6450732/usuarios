package com.example.project_plazoleta.infrastructure.out.jpa.adapter;

import com.example.project_plazoleta.domain.model.User;
import com.example.project_plazoleta.domain.spi.UserPersistencePort;
import com.example.project_plazoleta.infrastructure.exception.EmailNotFoundException;
import com.example.project_plazoleta.infrastructure.exception.UserAlreadyExistsException;
import com.example.project_plazoleta.infrastructure.exception.UserNotFoundException;
import com.example.project_plazoleta.infrastructure.out.jpa.entity.UserEntity;
import com.example.project_plazoleta.infrastructure.out.jpa.mapper.UserEntityMapper;
import com.example.project_plazoleta.infrastructure.out.jpa.repository.UserRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class UserJpaAdapter implements UserPersistencePort {

    private final UserRepository userRepository;

    private final UserEntityMapper userEntityMapper;

    @Override
    public void saveOwnerFromAdmin(User user) {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new UserAlreadyExistsException("The email is already used. Please choose another");
        }
        userRepository.save(userEntityMapper.toEntity(user));
    }

    @Override
    public void saveWorkerFromOwner(User user) {

        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new UserAlreadyExistsException("The email is already used. Please choose another");
        }
        userRepository.save(userEntityMapper.toEntity(user));

    }

    @Override
    public void saveClient(User user) {
        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new UserAlreadyExistsException("The email is already used. Please choose another");
        }
        userRepository.save(userEntityMapper.toEntity(user));
    }

    @Override
    public List<User> getAllUsers() {
        List<UserEntity> userEntityList = userRepository.findAll();
        return userEntityMapper.toUserList(userEntityList);
    }

    @Override
    public User getUserByEmail(String email) {

        UserEntity userEntity = userRepository.findByEmail(email).orElseThrow(() -> new EmailNotFoundException("Email does not exists"));

        System.out.println(userEntity.toString());

        return userEntityMapper.toUser(userEntity);
    }

    @Override
    public User getUserById(Long id) {

        UserEntity userEntity = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User does not exists"));

        return userEntityMapper.toUser(userEntity);
    }

    @Override
    public User validateUser(String email, String password) {

        UserEntity userEntity = userRepository.findByEmail(email).orElseThrow(() -> new EmailNotFoundException("Email does not exists"));

        return userEntityMapper.toUser(userEntity);
    }


}
