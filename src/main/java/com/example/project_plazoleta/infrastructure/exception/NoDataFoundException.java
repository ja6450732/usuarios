package com.example.project_plazoleta.infrastructure.exception;

public class NoDataFoundException extends RuntimeException {
    public NoDataFoundException() { super();}
}
