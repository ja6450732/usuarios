package com.example.project_plazoleta.infrastructure.configuration.general;

import com.example.project_plazoleta.domain.api.RolServicePort;
import com.example.project_plazoleta.domain.api.UserServicePort;
import com.example.project_plazoleta.domain.spi.RolPersistencePort;
import com.example.project_plazoleta.domain.spi.UserPersistencePort;
import com.example.project_plazoleta.domain.usecase.RolUseCase;
import com.example.project_plazoleta.domain.usecase.UserUseCase;
import com.example.project_plazoleta.infrastructure.configuration.securityconfig.JwtTokenProvider;
import com.example.project_plazoleta.infrastructure.out.jpa.adapter.RolJpaAdapter;
import com.example.project_plazoleta.infrastructure.out.jpa.adapter.UserJpaAdapter;
import com.example.project_plazoleta.infrastructure.out.jpa.mapper.RolEntityMapper;
import com.example.project_plazoleta.infrastructure.out.jpa.mapper.UserEntityMapper;
import com.example.project_plazoleta.infrastructure.out.jpa.repository.RolRepository;
import com.example.project_plazoleta.infrastructure.out.jpa.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class BeanConfig {

    private final UserRepository userRepository;

    private final UserEntityMapper userEntityMapper;

    private final RolRepository rolRepository;

    private final RolEntityMapper rolEntityMapper;

    public BeanConfig(UserRepository userRepository, UserEntityMapper userEntityMapper, RolRepository rolRepository, RolEntityMapper rolEntityMapper) {
        this.userRepository = userRepository;
        this.userEntityMapper = userEntityMapper;
        this.rolRepository = rolRepository;
        this.rolEntityMapper = rolEntityMapper;
    }

    @Bean
    public UserPersistencePort userPersistencePort() {
        return new UserJpaAdapter(userRepository, userEntityMapper);
    }

    @Bean
    public UserServicePort userServicePort() {
        return new UserUseCase(userPersistencePort());
    }

    @Bean
    public RolPersistencePort rolPersistencePort() {
        return new RolJpaAdapter(rolRepository, rolEntityMapper);
    }

    @Bean
    public RolServicePort rolServicePort() {
        return new RolUseCase(rolPersistencePort());
    }

    @Bean
    public PasswordEncoder passwordEncoder() { return new BCryptPasswordEncoder(); }

    @Bean
    public JwtTokenProvider jwtTokenProvider() { return new JwtTokenProvider(); }

}
