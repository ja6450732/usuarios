package com.example.project_plazoleta.infrastructure.configuration.securityconfig;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

public class JwtTokenProvider {

    private final String secretKey = "f6dcaf4c85a6355b8c7bb77babd05579f05b27229a3b4f96100f97af245f7838";

    private final Long timeSectionInSeconds = 3600L;


    public String createToken(String username, Long idrol) {

        Claims claims = Jwts.claims().setSubject(username);
        claims.put("rol", idrol);

        Date dateNow = new Date();
        Date dateExpritation = new Date(dateNow.getTime() + timeSectionInSeconds * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(dateNow)
                .setExpiration(dateExpritation)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();

    }
    
}
