package com.example.project_plazoleta.domain.spi;

import com.example.project_plazoleta.domain.model.User;

import java.util.List;

public interface UserPersistencePort {

    void saveOwnerFromAdmin(User user);

    void saveWorkerFromOwner(User user);

    List<User> getAllUsers();

    void saveClient(User user);

    // List<User> getAllUsersByRol();

    User getUserByEmail(String email);

    User getUserById(Long id);

    User validateUser(String email, String password);

}
