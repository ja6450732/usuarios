package com.example.project_plazoleta.domain.spi;

import com.example.project_plazoleta.domain.model.Rol;

import java.util.List;

public interface RolPersistencePort {

    List<Rol> getAllRoles();

}
