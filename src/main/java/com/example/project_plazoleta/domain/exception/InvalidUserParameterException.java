package com.example.project_plazoleta.domain.exception;

public class InvalidUserParameterException extends IllegalArgumentException{

    public InvalidUserParameterException(String message) { super(message); }

}
