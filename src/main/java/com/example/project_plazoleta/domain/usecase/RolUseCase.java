package com.example.project_plazoleta.domain.usecase;


import com.example.project_plazoleta.domain.api.RolServicePort;
import com.example.project_plazoleta.domain.model.Rol;
import com.example.project_plazoleta.domain.spi.RolPersistencePort;

import java.util.List;

public class RolUseCase implements RolServicePort {

    private final RolPersistencePort rolPersistencePort;

    public RolUseCase(RolPersistencePort rolPersistencePort) {
        this.rolPersistencePort = rolPersistencePort;
    }

    @Override
    public List<Rol> getAllRoles() { return rolPersistencePort.getAllRoles(); }


}
