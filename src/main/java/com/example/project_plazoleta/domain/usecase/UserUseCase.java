package com.example.project_plazoleta.domain.usecase;

import com.example.project_plazoleta.domain.api.UserServicePort;
import com.example.project_plazoleta.domain.exception.InvalidUserParameterException;
import com.example.project_plazoleta.domain.model.User;
import com.example.project_plazoleta.domain.spi.UserPersistencePort;
import com.example.project_plazoleta.domain.validation.UserValidation;

import java.time.format.DateTimeParseException;
import java.util.List;

public class UserUseCase implements UserServicePort {

    private final UserPersistencePort userPersistencePort;

    public UserUseCase(UserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }

    @Override
    public void saveOwnerFromAdmin(User user) {

        try{
            UserValidation.dateBirthValidation(user);
        } catch (DateTimeParseException error){
            throw new InvalidUserParameterException("Enter a valid dateBirth, the format is YYYY-MM-DD");
        }

        UserValidation.notNullValidation(user);
        UserValidation.documentValidation(user);
        UserValidation.cellphoneValidation(user);
        UserValidation.emailValidation(user);
        UserValidation.passwordValidation(user);
        userPersistencePort.saveOwnerFromAdmin(user);
    }

    @Override
    public void saveWorkerFromOwner(User user) {

        UserValidation.documentValidation(user);
        UserValidation.cellphoneValidation(user);
        UserValidation.emailValidation(user);
        UserValidation.passwordValidation(user);
        userPersistencePort.saveWorkerFromOwner(user);
    }

    @Override
    public void saveClient(User user) {

        userPersistencePort.saveClient(user);
    }

    @Override
    public List<User> getAllUsers() {

        return userPersistencePort.getAllUsers();


    }

    @Override
    public User getUserById(Long id) {

        return userPersistencePort.getUserById(id);
    }

    @Override
    public User getUserByEmail(String email) {

        return userPersistencePort.getUserByEmail(email);
    }

    @Override
    public User validateUser(String email, String password) {

        return userPersistencePort.validateUser(email, password);
    }




}
