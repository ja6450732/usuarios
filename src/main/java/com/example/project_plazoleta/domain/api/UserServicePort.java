package com.example.project_plazoleta.domain.api;


import com.example.project_plazoleta.domain.model.User;

import java.util.List;

public interface UserServicePort {

    void saveOwnerFromAdmin(User user);

    void saveWorkerFromOwner(User user);

    void saveClient(User user);

    List<User> getAllUsers();

    User getUserById(Long id);

    User getUserByEmail(String email);

    User validateUser(String email, String password);

}

