package com.example.project_plazoleta.domain.api;

import com.example.project_plazoleta.domain.model.Rol;

import java.util.List;

public interface RolServicePort {

    List<Rol> getAllRoles();

}
