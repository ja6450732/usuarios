package com.example.project_plazoleta.domain.validation;

import com.example.project_plazoleta.domain.exception.InvalidUserParameterException;
import com.example.project_plazoleta.domain.model.User;
import java.time.LocalDate;
import java.time.Period;

public class UserValidation {

    public static void notNullValidation(User user){
        if (user.getName() == null || user.getName().isEmpty()) {
            throw new IllegalArgumentException("The field name cannot be blank");
        }
        if (user.getEmail() == null || user.getEmail().isEmpty()) {
            throw new IllegalArgumentException("The field email cannot be blank");
        }
        if (user.getLastName() == null || user.getLastName().isEmpty()) {
            throw new IllegalArgumentException("The field lastname cannot be blank");
        }
        if (user.getPassword() == null || user.getPassword().isEmpty()) {
            throw new IllegalArgumentException("The field password cannot be blank");
        }
    }

    public static void documentValidation(User user){
        if (!user.getDocument().matches("\\d{7,15}")) {
            throw new InvalidUserParameterException("The field document cannot be less than 7 and more than 15 characters and must be only numbers");
        }
    }
    public static void cellphoneValidation(User user) {
        if (!user.getCellphone().matches("^\\+?\\d{6,13}$")) {
            throw new InvalidUserParameterException("Enter a valid cellphone");
        }
    }

    public static void dateBirthValidation(User user){
        if (!user.getDateBirth().toString().matches("^(?:(?:19|20)\\d\\d)-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12][0-9]|3[01])$")) {
            throw new InvalidUserParameterException("Enter a valid dateBirth, the format is YYYY-MM-DD");
        }

        if(Period.between(user.getDateBirth(),LocalDate.now()).getYears() < 18) {
            throw new InvalidUserParameterException("User must be older than 18 years old ");
        }
    }

    public static void emailValidation(User user){
        if (!user.getEmail().matches("^[A-Za-z]+(\\.[A-Za-z]+)?@[A-Za-z]+\\.[A-Za-z]+$")) {
            throw new InvalidUserParameterException("Enter a valid email");
        }
    }

    public static void passwordValidation(User user){
        if (!user.getPassword().matches("^(?=.*[A-Za-z])(?=.*\\d).{6,}$")) {
            throw new InvalidUserParameterException("Enter a valid password");
        }
    }


}
