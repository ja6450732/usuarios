INSERT INTO users (name, last_name, document, cellphone, date_birth, email, password, rol)
VALUES
    ('John', 'Smith', '123456789', '+441234567890', '1990-01-15', 'john.smith@example.com', '$2a$12$nKnmExpOv2gpI5OtWiqCpeNTIdLsMapqHRTvvqwBOd5xHd7Zge.a.', 1),
    ('William', 'Johnson', '987654321', '+442345678901', '1985-06-20', 'william.johnson@example.com', '$2a$12$EJV4xXo9rXmeh.wcrX09C.dQTr9BaU9XK07T.CV.NN8Fuyh0hasPa', 1),
    ('David', 'Brown', '456789123', '+443456789012', '1992-03-10', 'david.brown@example.com', '$2y$10$8jeyD50AOqpMsE1ceLGNt.HhOON7UJJUeeHwuUiky5WlN5xvkY7ZG', 3),
    ('George', 'Taylor', '654321987', '+444567890123', '1988-09-05', 'george.taylor@example.com', '$2y$10$4wMuaTp.ik9a4WzAQ4itQubae58zmYrppV4RDDjhCNHBs/B2xsAHa', 4),
    ('Charles', 'Lee', '321987654', '+445678901234', '1995-11-30', 'charles.lee@example.com', '$2y$10$ZMoL//Li5ScJtdUNG0eIP.0mkSqndhJg9k9b9aPkRBjhxrGZu7T3O', 4),
    ('Emma', 'Wilson', '987654321', '+441234567890', '1991-04-25', 'emma.wilson@example.com', '$2y$10$3atbo9M.URHy1EziJBYXEuxGQ3xDxL1DJTP.afl6TWQlzYHGkbKSC', 4),
    ('Olivia', 'Davies', '456789123', '+442345678901', '1993-08-14', 'olivia.davies@example.com', '$2y$10$GbsUciPN9gYFQyoSgKUCV.KlYJIaJMGVsND67TkpWjr1P3JlwkCo2', 2),
    ('Sophia', 'Evans', '654321987', '+443456789012', '1990-12-08', 'sophia.evans@example.com', '$2y$10$VuC6zO15/KuTY9HaduzWyOAnengIZZtqV0xqPO7SXd/hzmmXYLPH.', 3),
    ('Ava', 'Johnson', '321987654', '+444567890123', '1987-02-17', 'ava.johnson@example.com', '$2y$10$0UdGX64oOTOE1IYi3dcCXu4xKcUqIRBwZU78NVArLl2Yf7rCIUxky', 4),
    ('Mia', 'Thomas', '123456789', '+445678901234', '1984-07-29', 'mia.thomas@example.com', '$2y$10$UTNcX3bXISBujmM0fzaHi.GImDxm/7lVJhQRHDfmZ47NmI0XP2erS', 4),
    ('Liam', 'Roberts', '987654321', '+441234567890', '1998-09-03', 'liam.roberts@example.com', '$2y$10$KPa9ckDVsoeadiRZH2y3mei6MxAqTBoUhvpfj2bsB6KD6Bx2vBnA2', 4),
    ('Noah', 'Williams', '456789123', '+442345678901', '1996-06-22', 'noah.williams@example.com', '$2y$10$hbdaOAUGji4sm7kdS6Kr2urM42vYxHI4hC/QuCChlFEcrH6pM8Syy', 2),
    ('Oliver', 'Jones', '654321987', '+443456789012', '1989-03-18', 'oliver.jones@example.com', '$2y$10$.EcYU84nV1zkgjY9WVBeMu.QkDcgnPphkxgDNp92TIwhc/PjdAHDe', 3),
    ('Ethan', 'Smith', '321987654', '+444567890123', '1994-10-11', 'ethan.smith@example.com', '$2y$10$a3/rowZFsCO3RKu4W9pR/O.P8ed5o7LL7fkTkB4hntiWRUZldA4FC', 4),
    ('Aiden', 'Brown', '123456789', '+445678901234', '1986-11-02', 'aiden.brown@example.com', '$2y$10$kKzzkscvoFUQp431QTO/gOTIPafBpZfMnavz39K8vrvPuoesHpmXq', 4),
    ('Amelia', 'Taylor', '987654321', '+441234567890', '1997-12-19', 'amelia.taylor@example.com', '$2y$10$epV6deQpMqa.YymsrZY41u/VIomOnS8AqM.pNzf8OUIT3CejiM61e', 4),
    ('Sophia', 'Wilson', '456789123', '+442345678901', '1999-02-09', 'sophia.wilson@example.com', '$2y$10$3.DdsyxOwbUGUBXrevSW.uIYy3ylBYws2BbRq1u66Q3YBbASBkcBm', 2),
    ('Mia', 'Davies', '654321987', '+443456789012', '1985-05-07', 'mia.davies@example.com', '$2y$10$CFIK1RQU3JJpyoyTIp.1EOBmED4rajrnNVzF69ZteaQ1Xt5H58Ada', 3),
    ('Olivia', 'Evans', '321987654', '+444567890123', '1988-08-26', 'olivia.evans@example.com', '$2y$10$sG4Mn2q8hulutTIytBgPkOhZa.lIoXj7VnsvVuOgl.fCRCktYEgeS', 4),
    ('Emma', 'Johnson', '123456789', '+445678901234', '1996-09-15', 'emma.johnson@example.com', '$2y$10$TW16.QR2No2nJ/tuCMFkoe.oAajqHBXFSVzWXIGF2pOk8kMoCJmua', 4),
    ('Liam', 'Lee', '987654321', '+441234567890', '1994-10-04', 'liam.lee@example.com', '$2y$10$Pufu8PrR1aCsd0Xk4zimTOywHD6ZEnppVLw08V1XdVIENt4EXmAPK', 3),
    ('Noah', 'Thomas', '456789123', '+442345678901', '1987-11-23', 'noah.thomas@example.com', '$2y$10$PihVL1Cav77TPm2I4wpDnenwseMU9pIugdp6j7Cewb0SnF7Kf4JY2', 2),
    ('Oliver', 'Roberts', '654321987', '+443456789012', '1999-01-12', 'oliver.roberts@example.com', '$2y$10$/KLyunPZ3nwp9CosyMuuU.EHQsyobQOTUn8pXTl6vN94V.8XtC.Yq', 3),
    ('Ethan', 'Brown', '321987654', '+444567890123', '1992-02-01', 'ethan.brown@example.com', '$2y$10$KGigf/ppEeAZKqnLeOVy.eRwA4GV31y/xNdMIzrAQLX4wW8x3VvZK', 4),
    ('Aiden', 'Smith', '123456789', '+445678901234', '1986-03-21', 'aiden.smith@example.com', '$2y$10$Hq6X/s/OE86AZh0Az6KsKeLgHxDm.gRfZjxecP0V1oZSBy7bT9UNq', 4),
    ('Amelia', 'Taylor', '987654321', '+441234567890', '1991-04-10', 'amelia.taylor@example.com', '$2y$10$b9/qcKVTpR8Zab0dkceO9OiRZuiWxav8hpZZBjeTBHRq0hqIQfR.K', 3),
    ('Sophia', 'Wilson', '456789123', '+442345678901', '1993-05-29', 'sophia.wilson@example.com', '$2y$10$aK8Fp2IjWkskNqe7V/a.q.R2oKHFxoApKAPxjqrCNjOJpk29DDGZC', 2),
    ('Mia', 'Davies', '654321987', '+443456789012', '1988-06-18', 'mia.davies@example.com', '$2y$10$HfT18FDz8c6Bcohazxco0uJnvRx62RpBd1iodbCygwWAgZxUraO5W', 3),
    ('Olivia', 'Evans', '321987654', '+444567890123', '1995-07-07', 'olivia.evans@example.com', '$2y$10$F42RSEDVIhKUvN5zprxObuQcpOcJy9clLoiuf.m7859aO6YRpR/ea', 4),
    ('Emma', 'Johnson', '123456789', '+445678901234', '1984-08-26', 'emma.johnson@example.com', '$2y$10$DCU7dmR/0sPMv7s9cbLCi.PaC6LBNeTo8VYnNJv/0Oxlcd1mhazia', 4);

{
    "name": "Papayo",
    "lastName": "Johanson",
    "document": "987653321",
    "cellphone": "+432345678901",
    "dateBirth": "1985-06-20",
    "email": "papayo.johnson@example.com",
},

PARA AUTHENTICACION:

ADMIN
john.smith@example.com
Password123

OWNER
olivia.davies@example.com
Davies456

WORKER
david.brown@example.com
Pass123Word


Passwords: ["Password123"
            "Password456"
            "Pass123Word"
            "Word456Pass"
            "Pa55word789"
            "Wilson123"
            "Davies456"
            "Evans123"
            "Johnson456"
            "Thomas123"
            "Roberts123"
            "Williams456"
            "Jones123"
            "Smith123"
            "Brown123"
            "Taylor123"
            "Wilson456"
            "Davies123"
            "Evans456"
            "Johnson123"
            "Lee123"
            "Thomas456"
            "Roberts123"
            "Brown456"
            "Smith123"
            "Taylor456"
            "Wilson123"
            "Davies456"
            "Evans123"
            "Johnson456"
            "Validpasword321"
            "papaYO99"
            "Francesa123"]